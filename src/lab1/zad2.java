package lab1;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

import static java.lang.Math.*;

public class zad2 {

 public static void main(String[] args) throws FileNotFoundException {
	 File file = new File("slowa.txt");
	 Scanner bfr = new Scanner(file);
	 
	 String tekst = bfr.nextLine();
	 String wyraz[] = null;
	 
	 wyraz = tekst.split(",");
	 int ilosc_wyrazow = wyraz.length;
	 
	 int ilosc_trojkatow = 0;
	 
	 for(int i = 0; i < ilosc_wyrazow; i++)
	 {
		 String s�owo = wyraz[i].substring(1, wyraz[i].length()-1); // obetnij cudzyslow
		 
		 int dlugosc_slowa = s�owo.length();
		 int suma = 0;
		 
		 for(int j = 0; j < dlugosc_slowa; j++)
		 {
			suma += s�owo.charAt(j) - 'A' + 1;
		 }
		 
		 int pierw = (int)sqrt(suma*2);
		   if(pierw*(pierw+1)/2 == suma)
			   ilosc_trojkatow++;
	 }

        		   /*
        		    * Skoro wyraz jest �redni� dw�ch kolejnych liczb, to pierwiastek takiego
        		    * wyrazu (wzoru na enty wyraz) b�dzie minimalnie wi�kszy ni� mniejsza z tych liczb
        		    * Dlatego bior� pierwiastek podwojonej sumy i go zaokr�glam w d�,
        		    * nast�pnie sprawdzam czy wyraz przez niego utworzony jest zgodny z naszym badanym.
        		    * Algorytm sam wymy�li�em. Nie ma ogranicze� w d�ugo�ci wyrazu, co ma miejsce, gdyby
        		    * korzysta� z tablicy.
        		    * 
        		    * Lepiej korzysta� z matematyki ni� RAMu i procesora.
        		    */

       System.out.println("Ilosc takich slow: " + ilosc_trojkatow);
       bfr.close();
    }
 
}

