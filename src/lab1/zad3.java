package lab1;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class zad3 {
	static File file;
	static Scanner bfr;

 public static void main(String[] args) throws FileNotFoundException {
	 file = new File("poker.txt");
     bfr = new Scanner(file);
     
     String wynik1, wynik2;
     int gracz1=0;
     
     //System.out.println(ciag("A5432"));
     while(true)
     {
    	 wynik1 = sprawdz();
         wynik2 = sprawdz();
         
         if(wynik1 == "Error" || wynik2 == "Error")
        	 break;
         else
         {
        	 //System.out.println(wynik1);
             //System.out.println(wynik2);
             
             if(Porownaj(wynik1, wynik2))
            	 gracz1++;
         }
         
         
     }
     
     System.out.println("Gracz 1 wygra�: " + gracz1);
     bfr.close();
    }
 
 
 
 
 
 static String sprawdz(){
		/*
		 * Funkcja zwraca 6znakowego Stringa, kt�rego struktura wygl�da nast�puj�co:
		 * VXXXXX, gdzie V to warto�� punktowa uk�adu (im wy�sza, tym lepszy uk�ad, V={0-8}),
		 * a X to kolejne, posortowane kluczowe karty uk�adu. Pewne karty s� pomini�te,
		 * je�eli s� zb�dne (np. w przypadku fulla tylko istotna jest informacja jaka jest tr�jka)
		 * 
		 * Funkcja sama zczytuje karty ze strumienia, je�eli jest jego koniec, to funkcja zwraca "Error"
		 */
		
		 String wynik = "0";
		 String[] Karty = new String[5];
		 if(!bfr.hasNext())
			 return "Error";
		 
		 for(int i = 0; i < 5; i++)
	     {
	    	 Karty[i] = bfr.next();
	     }
		 String wartosci = SortujKarty(Param(Karty));
		 /*
		  * Ustawia karty wg kolejnosci, jest to bardzo pomocne przy sprawdzaniu czy jest to jaki� uk�ad
		  */
		 
		 if(kolor(Karty)) // je�eli kolor
			 {
			 	wynik = "5";
			 	//System.out.println("kolor " + wartosci);
			 }
		 
		 if(ciag(wartosci))
		 {
			 wynik = "4";
			 //System.out.println("strit " + wartosci);
		 }
			 
		 if(kolor(Karty) && ciag(wartosci)) // je�eli poker
			 wynik = "8";
		 
		 String A = Pary(wartosci);			
		 if(A.charAt(0) == 'P')				//jezeli jest tam jaki� uk�ad inny ni� WysokaKarta i �aden z powy�szych
		 {
			 char znak = A.charAt(1); 			// Kod znaku uk�adu1
			 String nowy = A.substring(2); 		// dane bez nag��wka
			 int dlugosc = nowy.length(); 		// dlugosc "reszty" po pierwszym ukladzie
			 
			 String A_2 = Pary(nowy);
			 
			 if(A_2.charAt(0) != 'P')			// jezeli nie ma drugiej pary/ tr�jki, to dodaj 2 znaki, �eby program si� nie wysypa�
				 A_2 = "YY" + A_2;
			 
			 char znak_2 = A_2.charAt(1);		// Kod znaku uk�adu2
			 String nowy_2 = A_2.substring(2);	// wlasciwy zwrocony wynik
			 int dlugosc_2 = nowy_2.length();	// dlugosc drugiej wlasciwej czesci
			 
			 switch(dlugosc_2){
			 case 0:				// full
				 if(dlugosc==3)
					 znak = znak_2;	// jezeli trojka w tym fullu byla druga para
				 wynik = "6";
				 wartosci = String.valueOf(znak) + "0000";
				 //System.out.println("full " + wartosci);
				 break; 		
			 case 1: 				// dwie pary / kareta
				 if(dlugosc == 3) 	// dwie pary
				 {
					 if(WartoscKarty(znak) < WartoscKarty(znak_2))
					 {
						 char t = znak;
						 znak = znak_2;
						 znak_2 = t;
					 }
					 wynik = "2";
					 wartosci = String.valueOf(znak) + String.valueOf(znak) + String.valueOf(znak_2) + String.valueOf(znak_2) + nowy_2;
					 //System.out.println("dwie pary " + wartosci);
				 }
				 else				// kareta
				 {
					 wynik = "7";
					 wartosci = String.valueOf(znak) + "0000";
					 //System.out.println("kareta " + wartosci);
				 }
				 
				 break; 		
			 case 2: 				// trips
				 wynik = "3";
				 wartosci = String.valueOf(znak) + "0000";
				 //System.out.println("trips " + wartosci);
				 break;			
			 case 3: 				// jedna para
				 wynik = "1";
				 wartosci = String.valueOf(znak) + String.valueOf(znak) + nowy;
				 //System.out.println("jedna para " + wartosci);
				 break;			
			 }

		 }
		 
		 
		 return wynik+wartosci;
	 }
 

static boolean Porownaj(String A, String B){ // musi miec 6 znak�w, pierwszy to wartosc ukladu
	/*
	 * Zwraca true, je�eli pierwszy gracz wygra�,
	 * zwraca false dla remisu lub przegranej
	 */
	for(int i = 0; i < 6; i++)
		if(WartoscKarty(A.charAt(i)) != WartoscKarty(B.charAt(i)))
			if(WartoscKarty(A.charAt(i)) > WartoscKarty(B.charAt(i)))
				return true;
			else
				return false;
	
	return false; // gdyby byly calkowicie takie same
 }

static boolean kolor(String[] K){
	/*
	 * Zwraca true, je�eli podany na wej�ciu uk�ad jest kolorem
	 */
	String moj = K[0];
	char znak = moj.charAt(1);
	for(int i = 1; i < 5; i++)
	{
		moj = K[i];
		if(moj.charAt(1) != znak)
			return false;
	}
	return true;
}

static boolean ciag(String K){
	/*
	 * Zwraca true, je�eli uk�ad jest stritem, dziala r�wnie� dobrze dla A5432
	 */
	int znak = WartoscKarty(K.charAt(0));
	int temp;
	
	for(int i = 1; i < 5; i++)
	{
		temp = WartoscKarty(K.charAt(i));
		// je�eli nastepny znak nie jest mniejszy o jeden i jednoczesnie nie jest to warunek
		if(temp != znak-1 && !(temp == 5 && znak == 0xE)) 
			return false;
		znak = temp;
	}
	
	return true;
}


static String Pary(String K){
	/*
	 * Funkcja zwraca uk�ad, je�eli nie ma �adnych par, tr�jek, etc.
	 * Je�eli jest jaka� para/tr�jka/kareta, to zwraca 'P'+Z+R,
	 * gdzie Z to znak, kt�ry tworzy ten uk�ad, a R, to pozosta�o�� uk�adu po usuni�ciu kart Z,
	 * zawsze parsowany jest jeden uk�ad (je�eli s� dwie pary, to tylko jedna zostaje przetworzona)
	 * 
	 * Przyk�ady:
	 * AJ842 -> AJ842
	 * AJJJ2 -> PJA2
	 * AKK22 -> PKA22
	 * A4432 -> P4A32
	 * 
	 */
	char znak = K.charAt(0);
	int ilosc_pow = 1;
	char temp;
	int dl = K.length();
	
	for(int i = 1; i < dl; i++)
	{
		temp = K.charAt(i);
		
		if(WartoscKarty(temp) == WartoscKarty(znak))
		{
			ilosc_pow++;
		}

		if(WartoscKarty(temp) != WartoscKarty(znak) || i == dl-1)
		{
			if(ilosc_pow>1)
			{
				StringBuffer bfr = new StringBuffer(K);
				if(i==dl-1)
					i++;
				bfr.delete(i-ilosc_pow, i); // tutaj to poprawi� i-ilosc_pow, i
				bfr.insert(0, "P" + znak);
				return bfr.toString();
			}
			ilosc_pow = 1;
			znak = temp;
		}

	}
	
	return K;
}


static String Param(String[] K){
	/*
	 * Zwraca parametry, czyli z wyraz�w typu AH, zwraca parametry typu A (warto�� kart, nie ich kolor)
	 * Na wej�cie jest podana tablica 5 kart, na wyj�ciu jest podany String sk�adaj�cy si� z wartosci kart,
	 * czyli na wyjsciu znajdzie sie np. AKT62
	 */
	String text;
	String wynik = "";
	for(int i = 0; i < 5; i++)
	{
		text = K[i];
		wynik += Character.toString(text.charAt(0));
	}
	
	return wynik;
}


static int WartoscKarty(char A){
	/*
	 * Zwraca warto�� karty, na wej�cie podane jest jej symbol, czyli zwraca tak� sam�
	 * warto�� dla znak�w 2-9, a dla T,J,Q,K,A - odpowiednie znaki w Hex'ach, zachowuj�c hierarchi�
	 */
	int w = A;
	w -= 48;
	switch(A){
	case 'A': return 0xE;
	case 'K': return 0xD;
	case 'Q': return 0xC;
	case 'J': return 0xB;
	case 'T': return 0xA;
	default: return w;
	}
}

static String SortujKarty(String K){
	/*
	 * Zwraca posortowane karty wg hierarchii
	 * 
	 * Przyk�ad:
	 * 62K3A -> AK632
	 * AKQJT -> AKQJT
	 * A2345 -> 5432A
	 * 29294 -> 99422
	 */
	int n = K.length();
	  for(int i=0;i<n;i++)
		  for(int j=0;j<n-i-1;j++) //p�tla wewn�trzna
		    if( WartoscKarty(K.charAt(j)) < WartoscKarty(K.charAt(j+1))) // j > j +1
		    {
		    	StringBuffer temp = new StringBuffer(K);
		    	temp.insert(j, temp.charAt(j+1));
		    	temp.delete(j+2, j+3);
		    	K = temp.toString();
		    }
	  
	return K;
}




}

